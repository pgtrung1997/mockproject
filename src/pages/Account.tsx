import React from "react";

function Account() {
  return (
    <React.Fragment>
      <div className="container mt-5 pt-5">
        <div className="row">
          <div className="col-md-12">
          <h3 className="text-center">Account</h3>

          </div>
          <div className="col-md-6">
            <img
              className="w-100"
              src="https://theme.hstatic.net/1000333436/1000696015/14/slideshow_1.jpg?v=345"
              alt=""
            />
          </div>
          <div className="col-md-6">
            <ul className="list-group list-group-flush">
              <li className="list-group-item">An item</li>
              <li className="list-group-item">A second item</li>
              <li className="list-group-item">A third item</li>
              <li className="list-group-item">A fourth item</li>
              <li className="list-group-item">And a fifth one</li>
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default Account;
