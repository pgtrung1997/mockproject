import React from "react";

function Header() {
  return (
    <React.Fragment>
      <section className="mainSlide">
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to="0"
              className="active"
            ></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src="https://theme.hstatic.net/1000333436/1000696015/14/slideshow_1.jpg?v=345"
                className="d-block w-100"
              ></img>
            </div>
            <div className="carousel-item">
              <img
                src="https://theme.hstatic.net/1000333436/1000696015/14/slideshow_1.jpg?v=345"
                className="d-block w-100"
              ></img>
            </div>
          </div>
        </div>
      </section>
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-6">
            <img
              className="w-100"
              src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_1_1024x1024.png?v=345"
            ></img>
          </div>
          <div className="col-md-6 ">
            <div className="row">
            <div className="col-md-12 d-flex">
              <div className="row">
              <div className="col-md-6">
                <img
                  className="w-100"
                  src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_2_1024x1024.png?v=345"
                ></img>
              </div>
              <div className="col-md-6">
                <img
                  className="w-100"
                  src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_3_1024x1024.png?v=345"
                ></img>
              </div>
              </div>
            </div>
            <div className="col-md-12 mt-3">
              <img
                className="w-100"
                src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_4_1024x1024.png?v=345"
              ></img>
            </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default Header;
