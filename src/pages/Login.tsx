import React from "react";
import { Formik, Field, Form, FastField, FormikProps } from "formik";
import InputField from "../common-ui/InputField";
import ButtonComponent from "../common-ui/ButtonComponent";
import * as Yup from "yup";
import { FormGroup, Container, Row } from "reactstrap";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { login } from "../store/user/action-creators";
// import * as UserModel from "../models/User";

export interface IFormValue {
  email: string;
  password: string;
  isShowPass: boolean;
}
function Login() {
  const history = useHistory();
  const dispatch = useDispatch();
  const initialValues: IFormValue = {
    email: "",
    password: "",
    isShowPass: false,
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().required("Trường email không được để trống"),
    password: Yup.string().required("Vui lòng nhập mật khẩu"),
  });
  const onSubmit = (value: any) => {
    const action = login(value,history);
    dispatch(action);
  };
  return (
    <Container>
      <Row className="d-flex justify-content-center align-items-center" xs="3">
        <Formik
          initialValues={initialValues}
          onSubmit={(values, { setSubmitting }) => {
            onSubmit(values);
            // setTimeout(() => {
            //   localStorage.setItem("accessToken", "123");
            //   history.push("/admin");
            //   alert(JSON.stringify(values, null, 2));
            //   setSubmitting(false);
            // }, 400);
          }}
          validationSchema={validationSchema}
        >
          {(formikProps: FormikProps<IFormValue>) => {
            return (
              <Form>
                <FastField
                  name="email"
                  component={InputField}
                  label="Email"
                  placeholder="Vui lòng nhập email"
                />
                <Field
                  name="password"
                  component={InputField}
                  label="Password"
                  placeholder="Vui lòng nhập mật khẩu"
                  type={formikProps.values.isShowPass ? "text" : "password"}
                />
                <FormGroup>
                  <label>
                    <Field type="checkbox" name="isShowPass" />
                    Check to display password
                  </label>
                </FormGroup>
                <ButtonComponent
                  type="submit"
                  name="Login"
                  bgColor="success"
                  disabled={!formikProps.isValid || !formikProps.dirty}
                />
              </Form>
            );
          }}
        </Formik>
      </Row>
    </Container>
  );
}
export default Login;
