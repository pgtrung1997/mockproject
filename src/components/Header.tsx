import React from "react";
import { BagCheck, Person } from "react-bootstrap-icons";

import { NavLink } from "react-router-dom";

function Header() {
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#">
          A D A M S T O R E 
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav m-auto">
            <li className="nav-item">
              <NavLink to="/home" className="nav-link">
                Trang chủ
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/about" className="nav-link">
                Giới thiệu
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/products" className="nav-link">
                Sản phẩm
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/news" className="nav-link">
                Tin tức
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/contact" className="nav-link">
                Liên hệ
              </NavLink>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
          </form>
          <NavLink to="/account" className="nav-link">
            <button className="btn btn-outline-info">
              <Person className="info" size={20} />
              <i className="bi bi-person"></i>
            </button>
          </NavLink>
          <NavLink to="/cart" className="nav-link">
            <button className="btn btn-outline-info">
              <BagCheck className="info" size={20} />
            </button>
          </NavLink>
        </div>
      </nav>
    </React.Fragment>
  );
}

export default Header;
