import React from "react";
import { Formik, Field, Form, FastField, FormikProps } from "formik";
import InputField from "../../common-ui/InputField";
import ButtonComponent from "../../common-ui/ButtonComponent";
import * as Yup from "yup";
import { FormGroup, Container, Row } from "reactstrap";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { updateUser } from "../../store/user/action-creators";
// import * as UserModel from "../models/User";

export interface IFormValue {
  currentEmail: string;
  newEmail: string;
  password: string;
  cfpassword: string;
  isShowPass: boolean;
}
function EditUserComponent() {
  const history = useHistory();
  const dispatch = useDispatch();
  const initialValues: IFormValue = {
    currentEmail:'',
    newEmail: "",
    password: "",
    cfpassword: "",
    isShowPass: false,
  };

  const validationSchema = Yup.object().shape({
    newEmail: Yup.string().required("Trường email không được để trống"),
    password: Yup.string().required("Vui lòng nhập mật khẩu"),
    cfpassword: Yup.string().oneOf(
        [Yup.ref("password"), null],
        "Mật khẩu không khớp. Vui lòng nhập lại"
      ),
  });
  const onSubmit = (value: any) => {
    const action = updateUser({
        currentEmail:value.currentEmail,
        newEmail : value.newEmail,
        password:value.password,
        numberPerpage:10
    }, history);
    dispatch(action);
  };
  return (
    <Container>
      <Row className="mt-5">
        <h3>Settings</h3>
      </Row>
      <Row className="mt-4" xs="3">
        <Formik
          initialValues={initialValues}
          onSubmit={(values, { setSubmitting }) => {
            onSubmit(values);
          }}
          validationSchema={validationSchema}
        >
          {(formikProps: FormikProps<IFormValue>) => {
            return (
              <Form>
                <FastField
                  name="currentEmail"
                  component={InputField}
                  label="Current Email"
                //   disabled
                />
                 <FastField
                  name="newEmail"
                  component={InputField}
                  label="New Email"
                />
                <Field
                  name="password"
                  component={InputField}
                  label="Password"
                  type={formikProps.values.isShowPass ? "text" : "password"}
                />
                  <Field
                  name="cfpassword"
                  component={InputField}
                  label="Confirm Password"
                  type={formikProps.values.isShowPass ? "text" : "password"}
                />
                <FormGroup>
                  <label>
                    <Field type="checkbox" name="isShowPass" />
                    Check to display password
                  </label>
                </FormGroup>
                <ButtonComponent
                  type="submit"
                  name="Login"
                  bgColor="success"
                  disabled={!formikProps.isValid || !formikProps.dirty}
                />
              </Form>
            );
          }}
        </Formik>
      </Row>
    </Container>
  );
}
export default EditUserComponent;
