import React from "react";
import {
  Formik,
  Field,
  Form,
  FastField,
  FormikProps,
  FieldArray,
  FieldArrayRenderProps,
} from "formik";
import InputField from "../common-ui/InputField";
import SelectField from "../common-ui/SelectField";
import MultiSelectField from "../common-ui/MultiSelectField";
import ButtonComponent from "../common-ui/ButtonComponent";
import * as Yup from "yup";
import { FormGroup } from "reactstrap";

const CATEGORY = [
  { value: 1, label: "Techology" },
  { value: 2, label: "Education" },
  { value: 3, label: "Nature" },
];
export interface IFormValue {
  email: string;
  password: string;
  cfpassword: string;
  singleSelect: string;
  multiSelect: string[];
  isShowPass: boolean;
  address: string[];
}
function FormTempalte() {
  const initialValues: IFormValue = {
    email: "",
    password: "",
    cfpassword: "",
    singleSelect: "",
    multiSelect: [],
    address: [],
    isShowPass: false,
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().required("Trường email không được để trống"),
    singleSelect: Yup.number().required("Vui lòng chọn danh mục"),
    cfpassword: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Mật khẩu không khớp. Vui lòng nhập lại"
    ),
  });
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
      validationSchema={validationSchema}
    >
      {(formikProps: FormikProps<IFormValue>) => {
        // const { values, errors, touched } = formikProps;
        return (
          <Form>
            <FastField
              name="email"
              component={InputField}
              label="Email"
              placeholder="Vui lòng nhập email"
            />
            <Field
              name="password"
              component={InputField}
              label="Password"
              placeholder="Vui lòng nhập mật khẩu"
              type={formikProps.values.isShowPass ? "text" : "password"}
            />
            <Field
              name="cfpassword"
              component={InputField}
              label="Confirm Password"
              placeholder="Nhập lại mật khẩu của bạn"
              type={formikProps.values.isShowPass ? "text" : "password"}
            />
            <FastField
              name="singleSelect"
              component={SelectField}
              label="Category"
              placeholder="Select option"
              options={CATEGORY}
            />
            <FastField
              name="multiSelect"
              component={MultiSelectField}
              label="Category Select"
              placeholder="Select multi option"
              options={CATEGORY}
            />
            <FieldArray name="address">
              {(arrayHelper: FieldArrayRenderProps) => (
                <div>
                  <div className="form-group">
                    <button
                      className="btn btn-secondary"
                      onClick={() => arrayHelper.push("")}
                    >
                      Add Adress
                    </button>
                  </div>
                  {formikProps.values.address.map((v, i) => (
                    <div>
                      <div key={i}>
                        <Field
                          className="form-control mb-1"
                          type="text"
                          name={`address[${i}]`}
                        />
                      </div>
                      <button
                        className="btn btn-danger"
                        onClick={() => arrayHelper.remove(i)}
                      >
                        Delete
                      </button>
                    </div>
                  ))}
                </div>
              )}
            </FieldArray>
            <FormGroup>
              <label>
                <Field type="checkbox" name="isShowPass" />
                Check to display password
              </label>
            </FormGroup>
            <ButtonComponent
              type="submit"
              name="saveLogin"
              bgColor="success"
              disabled={!formikProps.isValid || !formikProps.dirty}
            />
          </Form>
        );
      }}
    </Formik>
  );
}
export default FormTempalte;
