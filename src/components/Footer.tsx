import React from "react";

import style from './Footer.module.css'

function Footer() {
  return (
    <React.Fragment>
      <footer className={style.footer}>
        <div className="container mt-5 pt-5">
          <div className="row">
            <div className="col-md-4">
              <h4>A D A M S T O R E</h4>
              <p>
                lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor, sit
                amet consectetur adipisicing elit. Beatae architecto sequi
                cupiditate earum dolor?
              </p>
              <p>Phone: +84923716866</p>
              <p>Email: admin@adminstore.vn</p>
            </div>

            <div className="col-md-4">
              <h6>Dịch vụ khách hàng</h6>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">Tin tức</li>
                <li className="list-group-item">Liên hệ</li>
                <li className="list-group-item">Khuyến mãi</li>
                <li className="list-group-item">Adam's Video</li>
                <li className="list-group-item">Ưu đãi đối tác Adam</li>
              </ul>
            </div>

            <div className="col-md-4">
              <h6>Nhóm sản phẩm</h6>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">Giày da</li>
                <li className="list-group-item">Giày thể thao</li>
                <li className="list-group-item">Giày công sở</li>
                <li className="list-group-item">Giày thời trang</li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </React.Fragment>
  );
}
export default Footer;
