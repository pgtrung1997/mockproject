import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import About from "./pages/About";
import Account from "./pages/Account";
import Cart from "./pages/Cart";
import Contact from "./pages/Contact";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Products from "./pages/Products";
import News from "./pages/News";
import { BrowserRouter as Router, NavLink,Switch,Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <React.Fragment>
        <Header></Header>
        <div>
            <Switch>
              <Route path='/home' component={Home} />
              <Route path='/about' component={About} />
              <Route path='/cart' component={Cart} />
              <Route path='/contact' component={Contact} />
              <Route path='/login' component={Login} />
              <Route path='/products' component={Products} />
              <Route path='/register' component={Register} />
              <Route path='/account' component={Account} />
              <Route path='/news' component={News} />
            </Switch>
        </div>
        <Footer />
      </React.Fragment>
    </Router>
  );
}

export default App;
