export const ADD_USER = "ADD_USER";

export const FETCH_USER="FETCH_USER";
export const FETCH_USER_SUCCESS="FETCH_USER_SUCCESS";

export const LOGIN_USER="LOGIN_USER";
export const LOGIN_USER_SUCCESS="LOGIN_USER_SUCCESS";

export const LOGOUT="LOGOUT";

export const UPDATE_USER="UPDATE_USER";
export const UPDATE_USER_SUCCESS="UPDATE_USER_SUCCESS";