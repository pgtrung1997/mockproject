import * as Types from "./action-type";

const initialState: any = {
  users: [],
};

export function userReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.ADD_USER:
      return { ...state };
    case Types.LOGIN_USER_SUCCESS:
      const users = action.payload
      return { ...state,users };
    default:
      return state;
  }
}
