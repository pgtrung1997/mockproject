import { UPDATE_USER } from './action-type';
import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import * as Types from "../user/action-type";

const BASE_API = "http://localhost:3000";
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
const loginCallAPI = async (email: string, password: string) => {
  return await axios.post(BASE_API + "/api/login", {
    email: email,
    password: password,
  });
};
function* login(action: any) {
  try {
    const response: IResponseGenerator = yield loginCallAPI(
      action.payload.user.email,
      action.payload.user.password
    );
    localStorage.setItem("accessToken", response.data.token);
    localStorage.setItem("mailUser", response.data.email);
    action.payload.history.push("/customer/cardview");
    yield put({ type: Types.LOGIN_USER_SUCCESS, payload: response.data });
  } catch {
    //yiel put error
  }
}
const logoutCallAPI = async () => {
  return await axios.post(
    BASE_API + "/api/logout",
    {},
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
};
function* logout(action: any) {
  try {
    const response: IResponseGenerator = yield logoutCallAPI();
    if(response.data.success === true){
      localStorage.removeItem("accessToken");
      localStorage.removeItem("mailUser");
      action.history.push("/login");
    }
   
    // yield put({type:Types.LOGIN_USER_SUCCESS,payload:response.data});
  } catch {
    //yiel put error
  }
}


const updateUserCallAPI = async (user:any) => {
  return await axios.post(
    BASE_API + "/api/users",
    
      {user}
    ,
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
};
function* updateUser(action: any) {
  try {
    console.log(action);
    const response: IResponseGenerator = yield updateUserCallAPI(action.payload.user);
    console.log(response);
    if(response.data.success === true){
      localStorage.removeItem("accessToken");
      localStorage.removeItem("mailUser");
      localStorage.setItem("mailUser",action.payload.user.newEmail);
      action.history.push("/customer/cardview");
    }
   
    // yield put({type:Types.LOGIN_USER_SUCCESS,payload:response.data});
  } catch {
    //yiel put error
  }
}
export function* watcherFetchUser() {
  yield takeLatest(Types.LOGIN_USER, login);
  yield takeLatest(Types.LOGOUT, logout);
  yield takeLatest(Types.UPDATE_USER, updateUser);
}
