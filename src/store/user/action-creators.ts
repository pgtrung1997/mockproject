import * as Types from './action-type'


// redux saga
export const login=(user:any,history:any)=>({
    type: Types.LOGIN_USER,
    payload:{user,history}
})

export const logoutUser=(history:any)=>({
    type: Types.LOGOUT,
    history
})

export const updateUser=(user:any,history:any)=>({
    type: Types.UPDATE_USER,
    payload:{user,history}
})