import {all,fork} from 'redux-saga/effects'
import * as userSaga from './user/user.saga'
import {sagaProducts} from './customer/customer.saga'

export default function* rootSaga(){
    yield all([
        fork(userSaga.watcherFetchUser),
        fork(sagaProducts),
    ])
} 