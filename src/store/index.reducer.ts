import { combineReducers } from "redux";
import { userReducer } from "./user/user.reducer";
import {customerReducer} from './customer/customer.reducer';

export const indexReducer = combineReducers({
  userData: userReducer,
  customerData: customerReducer,
});
