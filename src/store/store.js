import { applyMiddleware,createStore } from "redux";
import { indexReducer } from "./index.reducer";
import createSagaMiddleware  from "@redux-saga/core";
import rootSaga from './index.saga'

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(indexReducer ,applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
