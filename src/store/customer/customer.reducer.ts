import * as Types from "./action-type";

const initialState: any = {
  customers: [],
  customer_detail: {},
};

export function customerReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.GET_CUSTOMER_SUCCESS:
      const customers = action.payload;
      return { ...state, customers };
    case Types.GET_CUSTOMER_DETAIL_SUCCESS:
      const customer_detail = action.payload;
      return { ...state, customer_detail };
    case Types.DELETE_CUSTOMER_SUCCESS:
      return state;
    default:
      return state;
  }
}
