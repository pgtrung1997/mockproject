import * as Types from './action-type'

// redux saga
export const getCustomer=(payload:number)=>({
    type: Types.GET_CUSTOMER,
    payload
})

export const getCustomerDetail=(payload:number)=>({
    type: Types.GET_CUSTOMER_DETAIL,
    payload
})

export const deleteCustomer=(id:number,history:any)=>({
    type: Types.DELETE_CUSTOMER,
    payload:{id,history}
})