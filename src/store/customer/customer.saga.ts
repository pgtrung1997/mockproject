import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import * as Types from "./action-type";

const USER_API = "http://localhost:3000";
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
const getCustomerCallAPI = async (page: number) => {
  return await axios.get(USER_API + "/api/customers/page/"+page+"/10");
};
function* getCustomer(action: any) {
  const response: IResponseGenerator = yield getCustomerCallAPI(action.payload);
  yield put({ type: Types.GET_CUSTOMER_SUCCESS, payload: response.data });
}

const getCustomerDetailCallAPI = async (id: number) => {
  return await axios.get(USER_API + "/api/customers/" + id);
};
function* getCustomerDetail(action: any) {
  const response: IResponseGenerator = yield getCustomerDetailCallAPI(
    action.payload
  );
  yield put({
    type: Types.GET_CUSTOMER_DETAIL_SUCCESS,
    payload: response.data,
  });
}

const deleteCustomerCallAPI = async (id: number) => {
  return await axios.delete(USER_API + "/api/customers/" + id, {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
};
function* deleteCustomer(action: any) {
  try {
    console.log(action);
    const response: IResponseGenerator = yield deleteCustomerCallAPI(
      action.payload.id
    );
    yield put({ type: Types.DELETE_CUSTOMER_SUCCESS });
    action.payload.history.push('/customer/cardview');
  } catch {
    // yiel put error
  }
}
export function* sagaProducts() {
  yield takeLatest(Types.GET_CUSTOMER, getCustomer);
  yield takeLatest(Types.GET_CUSTOMER_DETAIL, getCustomerDetail);
  yield takeLatest(Types.DELETE_CUSTOMER, deleteCustomer);
}
