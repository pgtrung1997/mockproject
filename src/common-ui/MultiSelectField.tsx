import { FieldProps } from "formik";
import { FormGroup, Label } from "reactstrap";
import Select, { OptionsType } from "react-select";
//  import makeAnimated from "react-select/animated";

//  const animatedComponents = makeAnimated();

interface Option {
  label: string;
  value: string;
}

interface CustomSelectProps extends FieldProps {
  options: OptionsType<Option>;
  isMulti?: boolean;
  className?: string;
  placeholder?: string;
  label?: string;
}

function MultiSelectField(Props: CustomSelectProps) {
  const { field, form, options, label, placeholder } = Props;
  const { name } = field;
  const {errors} = form;

  const handleSelectOptionChange: any = (option: Option[]) => {
    const selectedValue = option
      ? option.map((item: Option) => item.value)
      : option;
    const changeEvent = {
      target: {
        name: name,
        value: selectedValue,
      },
    };
    field.onChange(changeEvent);
  };

  return (
    <FormGroup>
      {label && <Label htmlFor={name}>{label}</Label>}

      <Select
        closeMenuOnSelect={false}
        isMulti
        onChange={handleSelectOptionChange}
        name={name}
        options={options}
        className="basic-multi-select"
        classNamePrefix="select"
        placeholder={placeholder}
      />
       {errors && <p className="text-danger">{errors[name]}</p>}
    </FormGroup>
  );
}

export default MultiSelectField;
