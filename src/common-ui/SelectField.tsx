import { ErrorMessage } from "formik";
import React from "react";
import Select from "react-select";
import { FormGroup, Label } from "reactstrap";

interface ISelectFiedlProps {
  field: any;
  form: any;

  label?: string;
  options: string[];
  placeholder?: string;
  disabled?: false;
}
function SelectField(Props: ISelectFiedlProps) {
  const { field, options, label, placeholder, disabled } = Props;
  const { name, value } = field;
  // const {errors,touched} = form;

  const selectedOption: any = options.find(
    (option: any) => option.value === value
  );
  const handleSelectOptionChange = (selectedOption: any) => {
    const selectedValue = selectedOption
      ? selectedOption.value
      : selectedOption;
    const changeEvent = {
      target: {
        name: name,
        value: selectedValue,
      },
    };
field.onChange(changeEvent);
  };
  return (
    <FormGroup>
      {label && <Label htmlFor={name}>{label}</Label>}
      <Select
        id={name}
        name={name}
        value={selectedOption}
        onChange={handleSelectOptionChange}
        placeholder={placeholder}
        isDisabled={disabled}
        options={options}
      />
      <ErrorMessage name ={name} component="div" className="text-danger" />
       {/* {errors && <p className="text-danger">{errors[name]}</p>} */}
    </FormGroup>
  );
}
export default SelectField;
