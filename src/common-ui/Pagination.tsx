import React, { useState, useEffect } from "react";

interface IPaginationProps {
  totalPage: number;
  changePage: (page: number) => void;
}
interface IPagination {
  page: number;
  status: boolean;
}
const setPagaStatus = (totalPage: number, page: number) => {
  var arr: any = [];
  for (let i = 1; i <= totalPage; i++) {
    var obj: IPagination = {
      page: i,
      status: i === page ? true : false,
    };
    arr.push(obj);
  }
  return arr;
};

function Pagination(Props: IPaginationProps) {
  const { totalPage } = Props;
  const [page, setPage] = useState(1);
  const [pagination, setPagination] = useState<any[]>([]);

  useEffect(() => {
    var arr = setPagaStatus(totalPage, page);
    setPagination([...arr]);
  }, [page]);

  const updatePage = (item: number)=>{
    Props.changePage(item);
  }
  const changePage = (item: number) => {
    setPage(item);
    updatePage(item);
  };
  const prevPage = () => {
    setPage(page - 1);
    updatePage(page - 1);
  };
  const nextPage = () => {
    setPage(page + 1);
    updatePage(page + 1);
  };
  return (
    <React.Fragment>
      <nav aria-label="Page navigation example">
        <ul className="pagination">
          <li className={`page-item ${page === 1 ? "disabled" : ""}`}>
            <a className="page-link" href="#" onClick={prevPage}>
              Previous
            </a>
          </li>

          {pagination.map((item) => {
            // if (!page.ellipsis) {
            return (
              <li key={item.page}>
                <li
                  className={item.status ? "page-item  active" : "page-item "}
                >
                  <a
                    className="page-link"
                    href="#"
                    onClick={(e) => changePage(item.page)}
                  >
                    {item.page}
                  </a>
                </li>
              </li>
            );
          })}
          <li className={`page-item ${page === totalPage ? "disabled" : ""}`}>
            <a className="page-link" href="#" onClick={nextPage}>
              Next
            </a>
          </li>
        </ul>
      </nav>
    </React.Fragment>
  );
}
export default Pagination;
