import React from "react";
import {ErrorMessage} from 'formik'
import { FormFeedback, FormGroup, Input, Label } from "reactstrap";

interface IInputFiedlProps {
  field: any;
  form: any;

  label?: string;
  type?: any;
  placeholder?: string;
  disabled?: false;
}

function InputField(Props: IInputFiedlProps) {
  const { field, form, type, label, placeholder, disabled } = Props;
  const { name, value, onChange, onBlur } = field;
  const {errors} = form;
  // const showError = errors[name] && touched[name];
  return (
    <React.Fragment>
      <FormGroup>
        {label && <Label htmlFor={name}>{label}</Label>}
        <Input
          id={name}
          name={name}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
          // isValid = {showError}
        />
        {errors && <p className="text-danger">{errors[name]}</p>}
        <ErrorMessage name={name} component = {FormFeedback} />
      </FormGroup>
    </React.Fragment>
  );
}
export default InputField;
