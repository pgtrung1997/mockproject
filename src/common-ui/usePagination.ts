import { useState } from "react";

interface IPaginationProps{
  itemsPage:number;
  data:string[];
  startFrom:number;
}

const usePagination = (Props: IPaginationProps) => {
  const { itemsPage, data, startFrom } = Props;
  const perPage = itemsPage ? itemsPage : 10;
  const pages = Math.ceil(data.length / perPage);
  const pagination = [];
  const [currentPage, setCurrentPage] = useState(
    startFrom <= pages ? startFrom : 1
  );
  const [sliceData, setSliceData] = useState(
    [...data].slice((currentPage - 1) * perPage, currentPage * perPage)
  );

  let ellipsisLeft = false;
  let ellipsisRight = false;
  for (let i = 1; i <= pages; i++) {
    if (i === currentPage) {
      pagination.push({ id: i, current: true, ellipsis: false });
    } else {
      if (
        i < 2 ||
        i > pages - 1 ||
        i === currentPage - 1 ||
        i === currentPage + 1
      ) {
        pagination.push({ id: i, current: false, ellipsis: false });
      } else if (i > 1 && i < currentPage && !ellipsisLeft) {
        pagination.push({ id: i, current: false, ellipsis: true });
      } else if (i < pages && i > currentPage && !ellipsisRight) {
        pagination.push({ id: i, current: false, ellipsis: true });
        ellipsisRight = true;
      }
    }
  }

  const changePage = (page: any, e: any) => {
    e.preventDefault();
    if (page !== currentPage) {
      setCurrentPage(page);
      setSliceData([...data].slice((page - 1) * perPage, page * perPage));
    }
  };
  const goToPrevPage = (e: any) => {
    e.preventDefault();
    setCurrentPage((prevVal: any) =>
      prevVal - 1 === 0 ? prevVal : prevVal - 1
    );
    if (currentPage !== 1) {
      setSliceData(
        [...data].slice(
          (currentPage - 2) * perPage,
          (currentPage - 1) * perPage
        )
      );
    }
  };
  const goToNextPage = (e: any) => {
    e.preventDefault();
    setCurrentPage((prevVal: any) =>
      prevVal === pages ? prevVal : prevVal + 1
    );
    if (currentPage !== pages) {
      setSliceData(
        [...data].slice(currentPage * perPage, (currentPage + 1) * perPage)
      );
    }
  };

  return {
    sliceData,
    pagination,
    prevPage: goToPrevPage,
    nextPage: goToNextPage,
    changePage,
  };
};
export default usePagination;
