import React from "react";
import { Button } from "reactstrap";
interface IButtonComponentProps {
  name: string;
  bgColor?: string;
  type?: any;
  onClick?: () => void;
  display?: string;
  disabled?: any;
}

function ButtonComponent(Props: IButtonComponentProps) {
  const onClick = () => {
    if (Props.onClick !== undefined) {
      Props.onClick();
    }
  };
  return (
    <React.Fragment>
      <Button
        type={Props.type}
        className={`btn btn-${Props.bgColor} ${
          Props.display && `d-${Props.display}`
        }`}
        onClick={onClick}
        disabled = {Props.disabled}
      >
        {Props.name}
      </Button>
    </React.Fragment>
  );
}

export default ButtonComponent;
